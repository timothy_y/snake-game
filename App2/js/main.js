﻿// Место для кода.

var canvas = document.getElementById("GameCanvas");
var ctx = canvas.getContext("2d");

var snakeLength = 5;

var x = canvas.width/2;
var y = canvas.height/2;

const speed = 1;

var dx = speed;
var dy = 0;

var upPressed = false;
var downPressed = false;
var leftPressed = false;
var rigntPressed = false;

var lastPos = new Array();
var positions = new Array();

var foodPos = {
    x: 0, y: 0
}

document.addEventListener("keydown", keyHandler, false);

ctx.fillStyle = "green";
ctx.fillRect(0, 0, canvas.width, canvas.height);

function keyHandler(e) {
    if (e.keyCode == 37) {
        dx = (() => { if (dx != speed) dx = -speed; dy = 0; return dx; })();
    }
    else if (e.keyCode == 38) {
        dy = (() => { if (dy != speed) dy = -speed; dx = 0; return dy; })();
    }
    else if (e.keyCode == 39) {
        dx = (() => { if (dx != -speed) dx = speed; dy = 0; return dx })();
    }
    else if (e.keyCode == 40) {
        dy = (() => { if (dy != -speed) dy = speed; dx = 0; return dy; })();
    }
}

function drawBall() {
    ctx.beginPath();
    lastPos.push({ x: x + 20 * dx, y: y + 20 * dy });
    ctx.strokeStyle = 'black';
    ctx.fillStyle = "blue";
    ctx.strokeRect(x + 20 * dx, y + 20 * dy, 20, 20);
    ctx.fillRect(x + 20 * dx, y + 20 * dy, 20, 20);
    x += 20 * dx;
    y += 20 * dy;
    ctx.closePath();
}

function cleanBall(posX, posY) { 

    ctx.beginPath();
    ctx.fillStyle = "green";
    ctx.fillRect(posX -1, posY - 1, 22, 22); 
    ctx.closePath();  
}

function drawFood() {
    foodPos = {
        x: Math.floor(Math.random() * (canvas.width)), y: Math.floor(Math.random() * (canvas.height))
    }
    ctx.beginPath();
    ctx.rect(foodPos.x, foodPos.y, 20, 20);
    ctx.fillStyle = "red";
    ctx.fill();
    ctx.closePath();
}

function eatFood() {
    ctx.beginPath();
    ctx.fillStyle = "green";
    ctx.fillRect(foodPos.x, foodPos.y, 20, 20);
    ctx.closePath();  
}
prompt("");

function draw() {

    if ((x <= foodPos.x + 10 && x >= foodPos.x - 10) && (y <= foodPos.y + 10 && y >= foodPos.y - 10)) {
        eatFood();
        snakeLength++;
        drawFood();      
    }
    
    drawBall();

    if (lastPos.length == snakeLength + 1) {
        cleanBall(lastPos[0].x, lastPos[0].y);
        lastPos.shift();
    }

    if (x > canvas.width || x < 0) {
        x = canvas.width - x; 
    }
    else if (y > canvas.height || y  < 0) {
        y = canvas.height - y;
    }

    if (detectCollision())
        close();        
}

function detectCollision() {

    for (var i = 0; i < lastPos.length - 1; i++)
        if ((x <= lastPos[i].x + 10 && x >= lastPos[i].x - 10) && (y <= lastPos[i].y + 10 && y >= lastPos[i].y - 10))
            return true;
    return false;
}

setInterval(draw, 100);
drawFood(); 